package eu.euas.wildtbe2.controllers;

import eu.euas.wildtbe2.components.ResultsComponent;
import eu.euas.wildtbe2.controller_services.APIKey;
import eu.euas.wildtbe2.controller_services.UserService;
import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@AllArgsConstructor
public class PostController {
    private final APIKey apiKey;
    private final UserService userService;
    private final ResultsComponent resultsComponent;

    @PostMapping(path = "results",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> getResult(@RequestHeader Map<String, String> headers,
                                            @RequestBody InputText inputText) {
        if (apiKey.isBadToken(headers)) {
            return apiKey.returnNoAccess();
        }
        return new ResponseEntity<>(resultsComponent.processToResult(userService.getUserUUID(headers), inputText),
                HttpStatus.CREATED);
    }
}
