package eu.euas.wildtbe2.controllers;

import eu.euas.wildtbe2.components.ProfileComponent;
import eu.euas.wildtbe2.components.StringsComponent;
import eu.euas.wildtbe2.controller_services.APIKey;
import eu.euas.wildtbe2.controller_services.UserService;
import eu.euas.wildtbe2.model.LangLevelAttempt;
import eu.euas.wildtbe2.model.Profile;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@AllArgsConstructor
public class GetController {

    private final APIKey apiKey;
    private final UserService userService;
    private final StringsComponent stringsComponent;
    private final ProfileComponent profileComponent;

    @GetMapping(path = "/strings")
    public ResponseEntity<List<String>> getString(@RequestHeader Map<String, String> headers,
                                                  @RequestParam int attempt, @RequestParam String lang,
                                                  @RequestParam String level) {
        if (apiKey.isBadToken(headers)) {
            return apiKey.returnNoAccess();
        }
        LangLevelAttempt langLevelAttempt = new LangLevelAttempt(userService.getUserUUID(headers),
                lang, level, attempt);
        return new ResponseEntity<>(stringsComponent.getPreparedText(userService.getUserUUID(headers),
                langLevelAttempt), HttpStatus.OK);
    }

    @GetMapping(path = "/profile")
    public ResponseEntity<Profile> getProfile(@RequestHeader Map<String, String> headers,
                                              @RequestParam String lang, @RequestParam String level) {
        if (apiKey.isBadToken(headers)) {
            return apiKey.returnNoAccess();
        }
        String userUuid = userService.getUserUUID(headers);
        LangLevelAttempt langLevelAttempt = new LangLevelAttempt(userUuid,
                lang, level, 0);
        return new ResponseEntity<>(profileComponent.getUserProfile(userUuid, langLevelAttempt), HttpStatus.OK);
    }
}
