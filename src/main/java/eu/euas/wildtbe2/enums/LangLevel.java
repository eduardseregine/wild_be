package eu.euas.wildtbe2.enums;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.LangLevelAttempt;
import lombok.Getter;

public enum LangLevel {
    DE_B1("deutchB1.txt"),
    DE_A2("deutchA2.txt"),
    EE_B1("estonianB1.txt"),
    EE_A2("estonianA2.txt");

    @Getter
    private String filename;
    LangLevel(String filename) {
        this.filename = filename;
    }

    public static LangLevel of(LangLevelAttempt langLevelAttempt) {
        return of(langLevelAttempt.getLang(), langLevelAttempt.getLevel());
    }

    public static LangLevel of(InputText inputText) {
        return of(inputText.getLang(), inputText.getLevel());
    }
    private static LangLevel of(String lang, String level) {
        if (lang.equals("DE")) {
            if (level.equals("B1")) {
                return DE_B1;
            }
            return DE_A2;
        }
        if (level.equals("B1")) {
            return EE_B1;
        }
        return EE_A2;
    }
}
