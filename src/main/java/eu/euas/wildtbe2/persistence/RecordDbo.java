package eu.euas.wildtbe2.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
public class RecordDbo {
    @Id
    @GeneratedValue
    Long id;
    String userUuid;
    LocalDateTime date;
    String lang;
    String level;
    Integer general;
    Integer words;
    Integer understanding;
    Integer attempts;
    Integer text;
}
