package eu.euas.wildtbe2.persistence;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LastTextService {

    private final static Map<String, Integer> lastTextsNumbers = new HashMap<>();

    public void save(String userUuid, int text) {
        lastTextsNumbers.put(userUuid, text);
    }

    public int getLastText(String userUuid) {
        if (lastTextsNumbers.containsKey(userUuid)) {
            return lastTextsNumbers.get(userUuid);
        }
        return 0;
    }
}
