package eu.euas.wildtbe2.persistence;

import eu.euas.wildtbe2.model.Record;
import org.springframework.stereotype.Service;


@Service
public class RecordMapper {
    Record map(RecordDbo dbo) {
        Record record = new Record();
        record.setId(dbo.getId());
        record.setUserUuid(dbo.getUserUuid());
        record.setDate(dbo.getDate());
        record.setLang(dbo.getLang());
        record.setLevel(dbo.getLevel());
        record.setGeneral(dbo.getGeneral());
        record.setWords(dbo.getWords());
        record.setUnderstanding(dbo.getUnderstanding());
        record.setAttempts(dbo.getAttempts());
        record.setText(dbo.getText());
        return record;
    }

    RecordDbo map(Record record) {
        RecordDbo dbo = new RecordDbo();
        dbo.setId(record.getId());
        dbo.setUserUuid(record.getUserUuid());
        dbo.setDate(record.getDate());
        dbo.setLang(record.getLang());
        dbo.setLevel(record.getLevel());
        dbo.setGeneral(record.getGeneral());
        dbo.setWords(record.getWords());
        dbo.setUnderstanding(record.getUnderstanding());
        dbo.setAttempts(record.getAttempts());
        dbo.setText(record.getText());
        return dbo;
    }
}
