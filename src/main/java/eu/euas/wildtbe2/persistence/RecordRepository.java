package eu.euas.wildtbe2.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RecordRepository extends CrudRepository<RecordDbo, Long> {

    @Query("SELECT r FROM RecordDbo r WHERE r.userUuid=:userUuid GROUP BY r.id order by r.lang")
    List<RecordDbo> findAllRecordsByUuid(String userUuid);

    @Query("SELECT r.text FROM RecordDbo r WHERE r.userUuid=:userUuid and r.lang=:lang and r.level=:level group by r.text")
    List<Integer> getAllTextsLangLevel(String userUuid, String lang, String level);
    @Query("SELECT COUNT (r.text) FROM RecordDbo r WHERE r.userUuid=:userUuid and r.lang=:lang and r.level=:level " +
            "and r.date>:from group by r.text, r.date")
    List<Integer> countTextsByUuidAndLangFromDate(String userUuid, String lang, String level, LocalDateTime from);

    @Query("SELECT AVG (r.general) FROM RecordDbo r WHERE r.userUuid=:userUuid and r.lang=:lang and r.level=:level " +
            "and r.date>:from group by r.general")
    List<Double> averageByUuidAndLangFromDate(String userUuid, String lang, String level, LocalDateTime from);
}
