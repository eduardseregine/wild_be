package eu.euas.wildtbe2.persistence;

import eu.euas.wildtbe2.model.Record;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class RecordService {

    private final RecordRepository recordRepository;
    private final RecordMapper recordMapper;

    public void save(Record record) {
        recordRepository.save(recordMapper.map(record));
    }

    public int getTexts(String userUuid, String lang, String level, LocalDateTime from) {
        Integer texts = recordRepository.countTextsByUuidAndLangFromDate(userUuid, lang, level, from).stream()
                .mapToInt(x->x).sum();
        if (texts == null) {
            return 0;
        }
        return texts;
    }

    public int getSuccess(String userUuid, String lang, String level, LocalDateTime from) {
        Double average = recordRepository.averageByUuidAndLangFromDate(userUuid, lang, level, from)
                .stream().mapToDouble(x->x).average().orElse(0);
        return (int) Math.round(average);
    }

    public List<Integer> getTextNumbers(String userUuid, String lang, String level) {
        return recordRepository.getAllTextsLangLevel(userUuid, lang, level);
    }

    @Modifying
    public void deleteAll() {
        recordRepository.deleteAll();
    }
}
