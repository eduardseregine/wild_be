package eu.euas.wildtbe2.utilities;

import eu.euas.wildtbe2.model.Lang;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TranslationUtility {

    private static final String URL = "https://api.tartunlp.ai/translation/v2";

    public String getTranslation(String text, Lang[] langs) {
        Connection.Response execute = null;
        Lang src = langs[0];
        Lang tgt = langs[1];
        text = "'" + text.trim() + "'";
        try {
            execute = Jsoup.connect(URL)
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .followRedirects(true)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .userAgent("Mozilla/5.0 AppleWebKit/537.36 (KHTML," +
                            " like Gecko) Chrome/45.0.2454.4 Safari/537.36")
                    .method(Connection.Method.POST)
                    .requestBody("{\n" +
                            "    \"text\": \"" + text + "\",\n" +
                            "    \"src\": \"" + src.getLang() + "\",\n" +
                            "    \"tgt\": \"" + tgt.getLang() + "\"\n" +
                            "}")
                    .maxBodySize(1_000 * 60)
                    .timeout(0) // infinite timeout
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return execute != null ? extractResult(execute.body()) : "No response";
    }

    private String extractResult(String result) {
        result = result.trim();
        if (result.startsWith("{\"result\":\"")) {
            result = result.substring(11);
        }
        if (result.endsWith("\"}")) {
            result = result.substring(0, result.length() - 2);
        }
        if (result.startsWith("'") && result.length() > 1) {
            result = result.substring(1);
        }
        if (result.endsWith("'") && result.length() > 1) {
            result = result.substring(0, result.length() - 1);
        }
        if (result.startsWith("\\") && result.length() > 2) {
            result = result.substring(2);
        }
        if (result.length() > 2 && result.charAt(result.length()-2) == '\\') {
            result = result.substring(0, result.length() - 2);
        }
        return result;
    }
}
