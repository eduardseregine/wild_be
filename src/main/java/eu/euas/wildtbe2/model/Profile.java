package eu.euas.wildtbe2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Profile {
    LocalDateTime start;
    LocalDateTime end;
    Integer texts;
    Integer success;
}
