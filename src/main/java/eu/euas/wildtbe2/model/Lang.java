package eu.euas.wildtbe2.model;

import lombok.Getter;

public enum Lang {
    ESTONIAN("est"),
    ENGLISH("eng"),
    RUSSIAN("rus"),
    GERMAN("ger");

    @Getter
    private String lang;

    Lang(String lang) {
        this.lang = lang;
    }
}
