package eu.euas.wildtbe2.model;

import lombok.Data;

@Data
public class InputText {
    String input;
    String lang;
    String level;
    String attempts;
}
