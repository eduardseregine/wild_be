package eu.euas.wildtbe2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LangLevelAttempt {
    String userUuid;
    String lang;
    String level;
    int attempt;
}
