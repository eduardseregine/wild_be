package eu.euas.wildtbe2.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class Record {
    Long id;
    String userUuid;
    LocalDateTime date;
    String lang;
    String level;
    Integer general;
    Integer words;
    Integer understanding;
    Integer attempts;
    Integer text;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return userUuid.equals(record.userUuid)
                && Objects.equals(lang, record.lang)
                && Objects.equals(level, record.level)
                && Objects.equals(general, record.general) && Objects.equals(text, record.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userUuid, lang, level, general, text);
    }

    public static Record of (String userUuid, InputText inputText, Result result) {
        Record record = new Record();
        record.setUserUuid(userUuid);
        record.setDate(LocalDateTime.now());
        record.setLang(inputText.getLang());
        record.setLevel(inputText.getLevel());
        record.setAttempts(result.getAttempts());
        record.setGeneral(result.getGeneral());
        record.setWords(result.getWords());
        record.setUnderstanding(result.getUnderstanding());
        record.setText(result.getTextId());
        return record;
    }
}
