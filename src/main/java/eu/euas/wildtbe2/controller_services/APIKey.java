package eu.euas.wildtbe2.controller_services;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Map;

@Service
@AllArgsConstructor
public class APIKey {

    private static final String HEADER_TOKEN_NAME = "authorization";
    private static String alg = "\"alg\":\"RS256\"";
    private final UserService userService;

    public boolean isBadToken(Map<String, String> headers) {
        if (!headers.containsKey(HEADER_TOKEN_NAME)) {
            return true;
        }
        String decoded = getDecodedToken(headers.get(HEADER_TOKEN_NAME));
        return userService.getUserUUID(headers).length() == 0
                || !decoded.contains(alg) || !decoded.contains("kid")
                || !decoded.contains("typ") || decoded.indexOf("typ") < decoded.indexOf("kid");
    }

    public ResponseEntity returnNoAccess() {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }


    private String getDecodedToken(String header) {
        if (header.startsWith("Bearer")) {
            String encodedResult = header.substring(6).split("\\.")[0];
            byte[] decodedBytes = Base64.getDecoder().decode(encodedResult);
            return new String(decodedBytes);
        }
        return "Nothing";
    }
}
