package eu.euas.wildtbe2.controller_services;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserService {

    public String getUserUUID(Map<String, String> headers) {
        String header = headers.get("authorization");
        if (header.startsWith("Bearer")) {
            String[] headerLine = header.substring(6).split("\\.");
            if (headerLine.length == 2) {
                return headerLine[1];
            }
        }
        return "";
    }
}
