package eu.euas.wildtbe2.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Service
public class SimilarWordsCounter {

    public int countSimilarWords(String text, String comparableText) {
        List<String> comparableWords = getWordsForComparison(comparableText);
        return (int) getWordsForComparison(text)
                .stream()
                .filter(x -> isContainWordSimilar(x, comparableWords))
                .count();
    }

    public int getTextLengthInWords(String text) {
        return getWordsForComparison(text).size();
    }

    private boolean isContainWordSimilar(String word, List<String> words) {
        return words.stream().anyMatch(x -> isWordSimilar(word, x));
    }

    private boolean isWordSimilar(String word1, String word2) {
        if (Math.abs(word1.length() - word2.length()) > 2) {
            return false;
        }
        return word1.contains(word2) || word2.contains(word1);
    }

    private List<String> getWordsForComparison(String text) {
        return Arrays.stream(text.split(" "))
                .map(this::removeNonLetters)
                .toList();
    }

    private String removeNonLetters(String word) {
        StringBuilder result = new StringBuilder();
        IntStream.range(0, word.length()).forEach(i -> {
            if (Character.isLetter(word.charAt(i))) {
                result.append(word.charAt(i));
            }
        });
        return result.toString().toLowerCase();
    }
}
