package eu.euas.wildtbe2.service;

import eu.euas.wildtbe2.enums.LangLevel;
import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;
import eu.euas.wildtbe2.persistence.LastTextService;
import eu.euas.wildtbe2.result_chain.ResultChainFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ResultService {

    private final LastTextService lastTextService;
    private final TextService textService;
    private final ResultChainFactory resultChainFactory;

    public Result getProcessedResult(String userUuid, InputText inputText) {
        int textId = lastTextService.getLastText(userUuid);
        Result result = new Result();
        result.setTextId(textId);
        resultChainFactory.getResultChain().handle(inputText, result, getText(textId, inputText));
        return result;
    }

    private String getText(int textId, InputText inputText) {
        LangLevel langLevel = LangLevel.of(inputText);
        return textService.getTextById(textId, langLevel);
    }
}
