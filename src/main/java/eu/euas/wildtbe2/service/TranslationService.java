package eu.euas.wildtbe2.service;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Lang;
import eu.euas.wildtbe2.utilities.TranslationUtility;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TranslationService {

    private final TranslationUtility translationUtility;
    private final SimilarWordsCounter similarWordsCounter;

    public int countSimilarInOriginal(String text, InputText inputText) {
        return getNumberOfSimilarWordsInTranslation(text, inputText.getLang());
    }
    public int countSimilarInInput(InputText inputText) {
        return getNumberOfSimilarWordsInTranslation(inputText.getInput(), inputText.getLang());
    }

    private int getNumberOfSimilarWordsInTranslation(String text, String lang) {
        String directTranslation = translationUtility.getTranslation(text, getLangs(lang));
        String backTranslation = translationUtility.getTranslation(directTranslation, getReversedLangs(getLangs(lang)));
        return similarWordsCounter.countSimilarWords(text, backTranslation);
    }

    private Lang[] getLangs(String lang) {
        if (lang.equals("DE")) {
            return new Lang[]{Lang.GERMAN, Lang.ENGLISH};
        }
        return new Lang[]{Lang.ESTONIAN, Lang.ENGLISH};
    }

    private Lang[] getReversedLangs(Lang[] langs) {
        return new Lang[]{langs[1], langs[0]};
    }
}
