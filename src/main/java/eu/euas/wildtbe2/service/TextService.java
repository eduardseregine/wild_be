package eu.euas.wildtbe2.service;

import eu.euas.wildtbe2.utilities.FileReaderUtility;
import eu.euas.wildtbe2.enums.LangLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class TextService {

    private final static Map<LangLevel, List<String>> texts = new HashMap<>();
    private final FileReaderUtility fileReaderUtility;

    public List<String> getWordsById(int id, LangLevel langLevel) {
        return splitToWords(getTextById(id, langLevel));
    }

    public String getTextById(int id, LangLevel langLevel) {
        update(langLevel);
        List<String> lines = texts.get(langLevel);
        return id > 0 & id < lines.size() ? lines.get(id) : lines.get(0);
    }

    private List<String> splitToWords(String text) {
        List<String> result = new ArrayList<>();
        String[] splitted = text.split(" ");
        for (String word : splitted) {
            while (word.length() > 0 && !Character.isLetter(word.charAt(0))) {
                result.add(String.valueOf(word.charAt(0)));
                word = word.substring(1);
            }
            List<String> toAdd = new ArrayList<>();
            while (word.length() > 0 && !Character.isLetter(word.charAt(word.length() - 1))) {
                toAdd.add(String.valueOf(word.charAt(word.length() - 1)));
                word = word.substring(0, word.length() - 1);
            }
            if (word.length() > 0) {
                result.add(word);
            }
            result.addAll(toAdd);
        }
        return result;
    }

    private void update(LangLevel langLevel) {
        if (!texts.containsKey(langLevel)) {
            texts.put(langLevel, fileReaderUtility.readTxt(langLevel.getFilename()));
        }
    }
}
