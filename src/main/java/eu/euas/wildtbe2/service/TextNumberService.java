package eu.euas.wildtbe2.service;

import eu.euas.wildtbe2.model.LangLevelAttempt;
import eu.euas.wildtbe2.persistence.LastTextService;
import eu.euas.wildtbe2.persistence.RecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
public class TextNumberService {
    private final static int MAX_NUMBER_TEXTS_INDEX = 10;
    private final RecordService recordService;
    private final LastTextService lastTextService;

    public int getTextNumber(String userUuid, LangLevelAttempt langLevelAttempt) {
        if (langLevelAttempt.getAttempt() > 0) {
            return lastTextService.getLastText(userUuid);
        }
        List<Integer> availableTextNumbers = getAvailableNumbers(recordService.getTextNumbers(userUuid, langLevelAttempt.getLang(),
                langLevelAttempt.getLevel()));
        if (availableTextNumbers.size() > 2) {
            return availableTextNumbers.get((int) (Math.random() * availableTextNumbers.size()));
        }
        return (int) (Math.random() * (MAX_NUMBER_TEXTS_INDEX + 1));
    }

    private List<Integer> getAvailableNumbers(List<Integer> oldTextNumbers) {
        return IntStream.range(0, MAX_NUMBER_TEXTS_INDEX)
                .filter(oldTextNumbers::contains)
                .boxed()
                .collect(Collectors.toList());
    }
}
