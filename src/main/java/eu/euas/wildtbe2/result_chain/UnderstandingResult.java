package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;
import eu.euas.wildtbe2.service.TranslationService;

public class UnderstandingResult implements ResultChain {

    private ResultChain next;

    private TranslationService translationService;

    public UnderstandingResult(ResultChain next, TranslationService translationService) {
        this.next = next;
        this.translationService = translationService;
    }
    @Override
    public void handle(InputText inputText, Result result, String text) {
        result.setUnderstanding(getUnderstanding(inputText, text));
        goToNext(inputText, result, text);
    }

    private void goToNext(InputText inputText, Result result, String text) {
        if (next != null) {
            next.handle(inputText, result, text);
        }
    }

    private int getUnderstanding (InputText inputText, String text) {
        int inputPercent = inputText.getInput().length() > 0 ? Math.min(100, translationService.countSimilarInInput(inputText) * 100
                / inputText.getInput().length()) : 0;
        int textPercent =  text.length() > 0 ? Math.min(100, translationService.countSimilarInOriginal(text, inputText) * 100
                / text.length()) : 0;
        int understanding = textPercent > 0 ? inputPercent * 100
                / textPercent : 0;
        int result = Math.min(100, (int) (understanding));
        return result > 15 ? result : 0;
    }
}
