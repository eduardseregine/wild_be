package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;

public class AttemptsResult implements ResultChain {

    private ResultChain next;

    public AttemptsResult(ResultChain next) {
        this.next = next;
    }
    @Override
    public void handle(InputText inputText, Result result, String text) {
        result.setAttempts(inputText.getAttempts().length() + 1);
        goToNext(inputText, result, text);
    }

    private void goToNext(InputText inputText, Result result, String text) {
        if (next != null) {
            next.handle(inputText, result, text);
        }
    }
}
