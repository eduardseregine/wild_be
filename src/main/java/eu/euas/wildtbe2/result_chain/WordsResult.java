package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;
import eu.euas.wildtbe2.service.SimilarWordsCounter;

public class WordsResult implements ResultChain {

    private ResultChain next;
    private SimilarWordsCounter similarWordsCounter;

    public WordsResult(ResultChain next, SimilarWordsCounter similarWordsCounter) {
        this.next = next;
        this.similarWordsCounter = similarWordsCounter;
    }
    @Override
    public void handle(InputText inputText, Result result, String text) {
        result.setWords(getWordsPercent(inputText, text));
        goToNext(inputText, result, text);
    }

    private void goToNext(InputText inputText, Result result, String text) {
        if (next != null) {
            next.handle(inputText, result, text);
        }
    }

    private int getWordsPercent(InputText inputText, String text) {
        int similarWordsCount = similarWordsCounter.countSimilarWords(inputText.getInput(), text);
        int wordsPercent = Math.min(100, similarWordsCount * 110 / similarWordsCounter.getTextLengthInWords(text));
        return wordsPercent > 15 ? wordsPercent : 0;
    }
}
