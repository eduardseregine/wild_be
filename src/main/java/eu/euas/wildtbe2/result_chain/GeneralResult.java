package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;

public class GeneralResult implements ResultChain {

    private ResultChain next;

    public GeneralResult(ResultChain next) {
        this.next = next;
    }
    @Override
    public void handle(InputText inputText, Result result, String text) {
        result.setGeneral(getGeneralResult(result));
        goToNext(inputText, result, text);
    }

    private void goToNext(InputText inputText, Result result, String text) {
        if (next != null) {
            next.handle(inputText, result, text);
        }
    }

    private int getGeneralResult(Result result) {
        int words = result.getWords();
        int understanding = result.getUnderstanding();
        int attempts = result.getAttempts();
        int wordsUnderstanding = Math.min(100, (int)((words + understanding) / 1.9));
        return wordsUnderstanding - 10 * Math.max(0, attempts - 1);
    }
}
