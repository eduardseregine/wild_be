package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.service.SimilarWordsCounter;
import eu.euas.wildtbe2.service.TranslationService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultChainFactory {

    @Getter
    final private ResultChain resultChain;

    private ResultChainFactory(@Autowired TranslationService translationService, @Autowired SimilarWordsCounter similarWordsCounter) {
        this.resultChain = new WordsResult(new UnderstandingResult(new AttemptsResult(new GeneralResult(null)),
                translationService), similarWordsCounter);
    }
}
