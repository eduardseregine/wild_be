package eu.euas.wildtbe2.result_chain;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Result;

public interface ResultChain {
        void handle(InputText inputText, Result result, String text);
}
