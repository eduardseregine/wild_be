package eu.euas.wildtbe2.components;

import eu.euas.wildtbe2.enums.LangLevel;
import eu.euas.wildtbe2.model.LangLevelAttempt;
import eu.euas.wildtbe2.persistence.LastTextService;
import eu.euas.wildtbe2.service.TextNumberService;
import eu.euas.wildtbe2.service.TextService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class StringsComponent {

    private final TextService textService;
    private final LastTextService lastTextService;
    private final TextNumberService textNumberService;
    public List<String> getPreparedText(String userUuid, LangLevelAttempt langLevelAttempt) {
        return getNextStrings(userUuid, langLevelAttempt);
    }

    private List<String> getNextStrings(String userUuid, LangLevelAttempt langLevelAttempt) {
        int textNumber = textNumberService.getTextNumber(userUuid, langLevelAttempt);
        lastTextService.save(userUuid, textNumber);
        return textService.getWordsById(textNumber, LangLevel.of(langLevelAttempt));
    }
}