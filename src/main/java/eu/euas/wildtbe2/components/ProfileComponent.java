package eu.euas.wildtbe2.components;

import eu.euas.wildtbe2.model.LangLevelAttempt;
import eu.euas.wildtbe2.model.Profile;
import eu.euas.wildtbe2.persistence.RecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class ProfileComponent {

    private final RecordService recordService;

    public Profile getUserProfile(String userUuid, LangLevelAttempt langLevelAttempt) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), 1, 0, 0).plusDays(1);
        return new Profile(from, LocalDateTime.now(), getTexts(userUuid, langLevelAttempt, from),
                getSuccess(userUuid, langLevelAttempt, from));
    }

    private int getTexts(String userUuid, LangLevelAttempt langLevelAttempt, LocalDateTime from) {
        return recordService.getTexts(userUuid, langLevelAttempt.getLang(), langLevelAttempt.getLevel(),
                from);
    }

    private int getSuccess(String userUuid, LangLevelAttempt langLevelAttempt, LocalDateTime from) {
        return recordService.getSuccess(userUuid, langLevelAttempt.getLang(), langLevelAttempt.getLevel(),
                from);
    }
}
