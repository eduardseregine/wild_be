package eu.euas.wildtbe2.components;

import eu.euas.wildtbe2.model.InputText;
import eu.euas.wildtbe2.model.Record;
import eu.euas.wildtbe2.model.Result;
import eu.euas.wildtbe2.persistence.RecordService;
import eu.euas.wildtbe2.service.ResultService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@AllArgsConstructor
public class ResultsComponent {

    private static final Map<String, Record> lastSaved = new HashMap<>();
    private final RecordService recordService;
    private final ResultService resultService;

    public Result processToResult(String userUuid, InputText inputText) {
        Result result = resultService.getProcessedResult(userUuid, inputText);
        save(userUuid, inputText, result);
        return result;
    }

    private void save(String userUuid, InputText inputText, Result result) {
        Record record = Record.of(userUuid, inputText, result);
        if (!isEqualsToLastSaved(userUuid, record)) {
            recordService.save(record);
        }
    }

    private boolean isEqualsToLastSaved(String userUuid, Record record) {
        if (lastSaved.containsKey(userUuid)) {
            if (lastSaved.get(userUuid).equals(record)) {
                return true;
            }
        }
        lastSaved.put(userUuid, record);
        return false;
    }
}
