package eu.euas.wildtbe2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Wildtbe2Application {

    public static void main(String[] args) {
        System.setProperty("SQL_TYPE","postgresql");
        System.setProperty("DRIVER_CLASS_NAME", "org.postgresql.Driver");
        System.setProperty("SQL_DIALECT","org.hibernate.dialect.PostgreSQL82Dialect");
        //TODO Replace with your credentials
        System.setProperty("SQL_ADDON_DB","");
        System.setProperty("SQL_ADDON_USER","");
        System.setProperty("SQL_ADDON_PASSWORD","");
        System.setProperty("CC_SQL_PROXYSQL_SOCKET_PATH","//:5432");

        SpringApplication.run(Wildtbe2Application.class, args);
    }

}
