Aga olin kindel, et oma tulevase naise peab isa ikka natuke rohkem ära tundma kui mina.
Rääkisin meie pere suurest uudisest esmaspäeval koolis oma parimale sõbrannale Meritile.
Me oleme Meritiga pinginaabrid alates esimesest klassist ja meil ei ole teineteise ees saladusi.
Pean tunnistama, et ega mul Meritile midagi põrutavat rääkida ole olnud.
Kuid siin meie sarnasus ka lõpeb. Nimelt on Meritil viimase viie aasta jooksul olnud vähemalt neli kasuisa.
Igal juhul tekitab see palju põnevaid ning sekka ka natuke kurbi olukordi, millest Merit mulle ikka räägib.
Merit jõuab uue kasuisaga vaevu tuttavaks saada, kui ta ema saab juba aru, et ta selle mehega  ei klapi.
Mul oli olnud juba paar päeva tunne, et isa plaan naist leida ei ole üldse nii tõsine.
Ja loomulikult olin ma olnud seetõttu veidi pettunud. Kuid nüüd paistis kõik jälle hästi olevat.
Ma tahan lihtsalt olla kindel, et seal nimekirjas ei ole kohe alguses mõnda sellist omadust, mis mulle ei meeldi.
Ilmselt minu põhjendus veenis teda. Oota! Ma prindin selle nimekirja kohe välja, ütles ta elutuppa kadudes.