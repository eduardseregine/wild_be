# About

RESTful backend for "Wir learn Deutsch by texten" application.   
Backend functionalities:  

 - GET request: provides a line of a German or an Estonian text on the request of frontend with an English translation  
   
 - POST request: on receiving the user's input from frontend, validates it by counting the number of the matching words in the original text with the input provided, as well as counting the number of matching words with translating an input into English and back to either German or Estonian  
   
  - within the same request: persistently stores the user's results  
    
 - GET request: provides an abstract from the persistence storage on the user's performance sorted by language and levev, as well as the posting date starting from the 1st day of the current month  

# Requirements
The fully fledged server uses the following:

Spring WEB Framework
SpringBoot
Postgresql (database credentials to be provided in the application.yml file) 
Hibernate

RESTful service is provided on port 8080.
Endpoints:
 - GET:  /input
 - POST: /result
 - GET: /final

# Building the project
You will need:

Java JDK 17 or higher
Gradle 7.4.0 or higher
Git
Clone the project and use Gradle to build the server

```
$ ./gradlew bootRun

```
